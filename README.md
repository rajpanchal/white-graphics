# White Graphics
This is simple website for fictional graphics and software company.

## Build With
1. HTML
2. CSS
3. JQuery

## Plugins Used
1. [Bootstrap v3.3.7](http://getbootstrap.com)
2. [Animate.css v3.5.2](http://daneden.me/animate)
3. [Owl Carousel v2.2.1](https://github.com/OwlCarousel2/OwlCarousel2)
4. [Isotope PACKAGED v3.0.4](http://isotope.metafizzy.co)
5. [Magnific Popup v1.1.0](http://dimsemenov.com/plugins/magnific-popup/)
6. [Waypoints v4.0.1](http://imakewebthings.com/waypoints/)

## License
MIT © [Raj Panchal](https://gitlab.com/rajpanchal)